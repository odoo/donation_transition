# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* product_analytic_donation
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.0\n"
"Report-Msgid-Bugs-To: \n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. module: product_analytic_donation
#: model:ir.model.fields,field_description:product_analytic_donation.field_donation_line__display_name
msgid "Display Name"
msgstr ""

#. module: product_analytic_donation
#: model:ir.model,name:product_analytic_donation.model_donation_line
msgid "Donation Lines"
msgstr ""

#. module: product_analytic_donation
#: model:ir.model.fields,field_description:product_analytic_donation.field_donation_line__id
msgid "ID"
msgstr ""

#. module: product_analytic_donation
#: model:ir.model.fields,field_description:product_analytic_donation.field_donation_line____last_update
msgid "Last Modified on"
msgstr ""
